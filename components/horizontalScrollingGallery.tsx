import MovieCard from "./movieCard"
import { useEffect, useRef } from 'react'

const HorizontalScrollingGallery = ({ moviesArray }) => {
  const listRef = useRef(null)
  const scrollLeftRef = useRef(null)
  const scrollRightRef = useRef(null)

  useEffect(() => {
    setScrollEventlisteners()
  }, [])

  const setScrollEventlisteners = () => {
    const scrollingElements = [scrollLeftRef.current, scrollRightRef.current]

    scrollingElements.forEach(scrollingElement => {
      scrollingElement.addEventListener('mouseenter', () => {
        // set repeating interval for smooth scrolling effect
        const scrollingInterval = setInterval(() => {
          const scrollSpeed = 12
          // define whether to scroll left or right depending on dataset value (value gets set to negative for right scroll)
          const scrollDirection = scrollingElement.dataset.scrolldirection === 'left' ? -scrollSpeed : scrollSpeed
          listRef.current.scrollLeft += scrollDirection
        }, 30)
        scrollingElement.addEventListener('mouseout', () => {
          clearInterval(scrollingInterval) // clear interval to stop scrolling
        })
      })
    })
  }

  return (
    <div className="position-relative">
      <div className="scroll-section left" data-scrolldirection='left' ref={scrollLeftRef}>
        <i className="fas fa-chevron-left"></i>
      </div>
      <div className="scroll-section right" data-scrolldirection='right' ref={scrollRightRef}>
        <i className="fas fa-chevron-right"></i>
      </div>
      <ul className="movies-list-scrollable" ref={listRef}>
        {moviesArray.map(movie => {
          return (
            <div key={movie.id}>
              <MovieCard movie={movie} />
            </div>
          )
        })}
      </ul>
    </div>
  )
}

export default HorizontalScrollingGallery
