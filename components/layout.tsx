import { useEffect } from "react"
import Navbar from "./navbar"
import SideBar from "./sidebar"

const sidebarContent = [
  {
    sectionName: 'movies',
    pages: [
      {
        pageUrl: '/movies/popular',
        buttonText: 'Most popular movies',
        icon: 'fas fa-trophy'
      },
      {
        pageUrl: '/movies/genres',
        buttonText: 'Browse by genre',
        icon: 'fas fa-theater-masks'
      },
      {
        pageUrl: '/movies/cinemas',
        buttonText: 'Find a cinema near me',
        icon: 'fas fa-map-marker-alt'
      }
    ]
  },
  {
    sectionName: 'TV shows',
    pages: [
      {
        pageUrl: '/',
        buttonText: 'Most popular shows',
        icon: 'fas fa-trophy'
      },
      {
        pageUrl: '/',
        buttonText: 'Browse by genre',
        icon: 'fas fa-theater-masks'
      }
    ]
  }
]

const Layout = ({ children }) => {

  const setContentWidth = () => {
    const sidebarElement = document.getElementById('sidebar')
    const mainContentElement = document.getElementById('main-content')
    const sidebarWidth = sidebarElement.clientWidth
    mainContentElement.style.marginLeft = `${sidebarWidth}px`
  }

  useEffect(() => {
    setContentWidth()
  }, [])

  return(
    <>
      <Navbar />
      <SideBar sidebarContent={sidebarContent}/>
      <main className="main-content" id='main-content'>{children}</main>
    </>
  )
}

export default Layout
