import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Chart.js Bar Chart',
    },
  },
};

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

const BarChart = ({ props }) => {
  const { chartData, labels } = props

  const data = {
    labels,
    datasets: [
      {
        label: 'Most Popular Movies',
        data: chartData,
        backgroundColor: 'rgba(255, 99, 132, 0.6)',
      },
    ],
  };
  return(
    <>
      <Bar options={options} data={data} />
    </>
  )
}

export default BarChart
