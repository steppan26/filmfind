import Image from 'next/image'

interface movie {
    id: number | string
    vote_average: string
    vote_count: string
    poster_path: string
    title: string
    overview: string

}

export default function MovieCard({ movie }): JSX.Element {

  const posterLoader = ({ src }) => (`http://image.tmdb.org/t/p/w300${src}`)

  return(
    <li className="movie-item" key={movie.id}>
      <div className="poster-wrapper card position-relative" style={{ width: '200px', height: '350px' }}>
        <a href={`/movies/${movie.id}`} style={{ height: '100%' }} className='position-relative'>
          <Image
            loader={posterLoader}
            className="card-img-top"
            src={movie.poster_path}
            alt={`${movie.title} poster`}
            layout="fill"
          />
          <div className="movie-details-wrapper card-body d-flex flex-column justify-content-between">
            <div className="info-wrapper">
              <p className="card-text text-muted">{movie.overview}</p>
            </div>
            <h6 className="movie-rating"><span>{Number.parseFloat(movie.vote_average).toFixed(1)}/10</span> ({movie.vote_count} votes)</h6>
          </div>
        </a>
      </div>
    </li>
  )
}
