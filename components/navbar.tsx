import Image from "next/image"
import { useRef, useState } from "react"
import Avatar from '../public/avatar.png'

const Navbar = () => {
  const [currentLanguage, setCurrentLanguage] = useState('fr')
  const navMenuAvatarList = useRef(null)
  const navMenuCountriesList = useRef(null)

  const changeCurrentLanguage = (language) => {
    setCurrentLanguage(language)
    console.log('changing site language to ', currentLanguage)
  }

  const toggleNavMenu = (target) => {
    const menusArray = [navMenuAvatarList.current, navMenuCountriesList.current]
    menusArray.forEach((menu) => {
      if (menu !== target) {
        menu.classList.add('hidden')
      }
    })
    target.classList.toggle('hidden')
  }

  return(
    <>
      <nav className="navbar-wrapper" id='navbar'>
        <div className="nav-menu-dropdown position-relative">
          <div className="countries-wrapper" onClick={() => toggleNavMenu(navMenuCountriesList.current)}>
            <img src={`/flags/4x3/${currentLanguage}.svg`} width={30} alt='french flag' />
          </div>

          <ul className="list-group position-absolute w-100 hidden" ref={navMenuCountriesList}>
            <li className="list-group-item" onClick={() => changeCurrentLanguage('fr')}>
              <img src='/flags/4x3/fr.svg' width={30} alt='french flag' />
            </li>
            <li className="list-group-item" onClick={() => changeCurrentLanguage('gb')}>
              <img src='/flags/4x3/gb.svg' width={30} alt='british flag' />
            </li>
          </ul>
        </div>
        <div className="nav-menu-dropdown position-relative">
          <div className="avatar-wrapper" onClick={() => toggleNavMenu(navMenuAvatarList.current)}>
            <Image
              src={Avatar}
              width={35}
              height={35}
              alt={'avatar'}
            />
            <h5>Stéphane ↓</h5>
          </div>

          <ul className="list-group position-absolute w-100 hidden" ref={navMenuAvatarList}>
            <li className="list-group-item">Menu 1</li>
            <li className="list-group-item">Menu 2</li>
            <li className="list-group-item">Menu 3</li>
            <li className="list-group-item">Menu 4</li>
          </ul>
        </div>
      </nav>
    </>
  )
}

export default Navbar
