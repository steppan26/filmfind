import Logo from '../public/filmfind_logo.png'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import { useRouter } from 'next/router'

interface Page {
  pageUrl: string
  buttonText: string
  icon: string
}

const SideBar = ({ sidebarContent }): JSX.Element => {
  const router = useRouter()
  return(
    <>
      <nav className='sidebar-wrapper' id='sidebar'>
        <div className="img-logo-wrapper">
          <a href="/" >
            <Image
              className='logo'
              src={Logo}
              alt='Filmfind logo'
            />
          </a>
        </div>
        { sidebarContent.map((section, index) => {
          return (
            <div key={index + section.sectionName}>
              <h3 className='sidebar-subtitle'>{section.sectionName}</h3>
              <ul>
                {section.pages.map((page: Page, index: number)=>{
                  return(
                    <div className={`btn btn-sidebar mt-5 text-center ${router.pathname === page.pageUrl ? ' active' : ''}`} key={index + page.pageUrl}>
                      <a href={page.pageUrl}>
                        <div>
                          <i aria-hidden className={`${page.icon} me-3`} />
                          {page.buttonText}
                        </div>
                        </a>
                    </div>
                  )
                })}
              </ul>
            </div>
          )
        })}
      </nav>
    </>
  )

}

export default SideBar
