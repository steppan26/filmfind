import Logo from '../public/filmfind_logo.png'
import Image from 'next/image'
import { useState, useEffect } from 'react'
import HorizontalScrollingGallery from '../components/horizontalScrollingGallery'

const apiKey = 'f48b34c17de48f2ea5cc1dcc45f4b07a'

export default function Home() {
  const [movies, setMovies] = useState([])

  useEffect(() => {
    fetchData()
  }, [])

  const fetchData = async () => {
    // fetch list of latest movies available
    const url = `https://api.themoviedb.org/3/discover/movie?api_key=${apiKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&year=2022&vote_average.gte=8`
    const res = await fetch(url)
    const data = await res.json()
    const moviesArray = data.results.filter(movie => movie.poster_path)
    setMovies(moviesArray)
  }

  return (
    <div className='container'>
      <div className="landing-banner">
        <div className='main-title'>
          <div className="avatar">
            <Image src={Logo} layout='responsive' />
          </div>
          <h1>Filmfind</h1>
        </div>
        <h1 className='text-center sub-title'>Find your joy</h1>
      </div>
      <div className='home-movies-latest'>
        <h2 className="text-center my-3">Latest Movies</h2>
        <HorizontalScrollingGallery moviesArray={movies} />
      </div>
    </div>
  )
}
