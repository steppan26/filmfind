import Link from "next/link"
import { useEffect, useRef, useState } from "react"
import HorizontalScrollingGallery from '../../components/horizontalScrollingGallery'

interface movieData {
  data: {
    id: number
    title: string
    poster_url: string
    overview: string
    vote_average: number
    vote_count: number
    [propName: string]: any
  }
  similarMoviesArray: Array<Object>
}

export async function getServerSideProps(ctx) {
  const { id } = ctx.query
  const url = `https://api.themoviedb.org/3/movie/${id}?api_key=f48b34c17de48f2ea5cc1dcc45f4b07a&language=en-US`
  const req = await fetch(url)
  const data = await req.json()
  const similarMoviesArray = await fetchSimilarMovies(id)
  return { props: { data, similarMoviesArray } }

}

const fetchSimilarMovies = async (movieId) => {
  const url = `https://api.themoviedb.org/3/movie/${movieId}/similar?api_key=f48b34c17de48f2ea5cc1dcc45f4b07a&language=en-US`
  const req = await fetch(url)
  const data = await req.json()
  return data.results
}


const Page = ({ data, similarMoviesArray }: movieData) => {
  const [movie] = useState(data)
  const [similarMovies] = useState(similarMoviesArray || [])

  const backdropRef = useRef(null)

  const landingStyle = {
    backgroundImage: `url("http://image.tmdb.org/t/p/original${movie['backdrop_path']}")`,
  }

  const getBottomOfNavBar = () => {
    return document.getElementById('navbar').scrollHeight
  }

  useEffect(()=> {
    const topOfElement: number = getBottomOfNavBar()
    backdropRef.current.style.top = `${topOfElement}px`
  }, [])

  return(
    <>
      <h1 className="movie-show-title">{movie.title}</h1>
      <div style={landingStyle} className="movie-show-backdrop" ref={backdropRef}>
      </div>
      <div className="container">
        <div className="position-relative">
          <p className="movie-show-overview">{ movie.overview }</p>
          <h2 className="text-center my-3 similar-movies-title">Similar Movies</h2>
          <HorizontalScrollingGallery moviesArray={similarMovies} />
        </div>
      </div>
    </>
  )
}

export default Page
