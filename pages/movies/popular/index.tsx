import { useEffect, useState } from "react"
import BarChart from "../../../components/barChart"
import MovieCard from "../../../components/movieCard"

interface ChartProps {

}

const Index = () => {
  const [movies, updateMovies] = useState([])
  const [chartProps, updateChartProps] = useState({ labels: [], chartData: [] })

  const compare = (a, b) => {
    if (a.vote_average < b.vote_average) {
      return 1;
    }
    if (a.vote_average > b.vote_average) {
      return -1;
    }
    return 0;
  }

  const fetchMovies = async() => {
    console.log('fetching movies')
    const req = await fetch('https://api.themoviedb.org/3/discover/movie?api_key=f48b34c17de48f2ea5cc1dcc45f4b07a&sort_by=popularity.desc')
    const data = await req.json()
    updateMovies(data.results.sort(compare))
  }

  const fetchChartData = () => {
    const labels = movies.map(movie => (movie.title))
    const chartData = movies.map(movie => (movie.vote_average))
    return {labels: labels, chartData: chartData}
  }

  useEffect(() => {
    fetchMovies()
    updateChartProps(() => fetchChartData())
  }, [])

  const posterLoader = ({ src }) => {
    return `http://image.tmdb.org/t/p/w300${src}`
  }

  return (
    <>
      <h1 className="text-center my-5">MOST POPULAR MOVIES</h1>
      <BarChart props={chartProps} />
      <ul className="d-flex flex-wrap movies-list">
        {movies.map(movie => {
          return(
            <>
              <MovieCard movie={movie} />
            </>
        )})}
      </ul>
    </>
  )
}

export default Index;
