import mapbox, { Map } from 'mapbox-gl/dist/mapbox-gl.js'
import { useEffect, useRef, useState } from 'react';

const apiToken = 'pk.eyJ1Ijoic3RlcHBhbiIsImEiOiJja3l4NjZjeHcwMXZ2MnVybmI2cjRzbXk2In0.NlPOZmDOR1aIrqQ25gm02g'

const Page = () => {
  const mapContainerRef = useRef(null)
  const inputFieldRef = useRef(null)
  const addressDropdownRef = useRef(null)

  const [cinemas, setCinemas] = useState([])
  const [map, setMap] = useState()

  useEffect(() => {
    initialiseMap()
  }, [])

  const initialiseMap = () => {
    mapbox.accessToken = apiToken
    const mapInstance = new Map({
      container: mapContainerRef.current,
      style: 'mapbox://styles/steppan/ckyx8a1wg005x14t83kdlxvjn',
      center: [2.33, 48.87],
      zoom: 11
    })
    setMap(mapInstance)
  }

  const addSearchLocationMarkerToMap = (coordinates = []) => {
    map.flyTo({
      center: coordinates,
      essential: true // this animation is considered essential with respect to prefers-reduced-motion
    });
    const customMarkerColor = '#F34457' // set the marker color
    const customMarker = document.createElement(`div`)
    customMarker.innerHTML = `<svg display="block" height="41px" width="27px" viewBox="0 0 27 41"><g fill-rule="nonzero"><g transform="translate(3.0, 29.0)" fill="#000000"><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="10.5" ry="5.25002273"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="10.5" ry="5.25002273"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="9.5" ry="4.77275007"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="8.5" ry="4.29549936"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="7.5" ry="3.81822308"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="6.5" ry="3.34094679"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="5.5" ry="2.86367051"></ellipse><ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="4.5" ry="2.38636864"></ellipse></g><g fill="${customMarkerColor}"><path d="M27,13.5 C27,19.074644 20.250001,27.000002 14.75,34.500002 C14.016665,35.500004 12.983335,35.500004 12.25,34.500002 C6.7499993,27.000002 0,19.222562 0,13.5 C0,6.0441559 6.0441559,0 13.5,0 C20.955844,0 27,6.0441559 27,13.5 Z"></path></g><g opacity="0.25" fill="#000000"><path d="M13.5,0 C6.0441559,0 0,6.0441559 0,13.5 C0,19.222562 6.7499993,27 12.25,34.5 C13,35.522727 14.016664,35.500004 14.75,34.5 C20.250001,27 27,19.074644 27,13.5 C27,6.0441559 20.955844,0 13.5,0 Z M13.5,1 C20.415404,1 26,6.584596 26,13.5 C26,15.898657 24.495584,19.181431 22.220703,22.738281 C19.945823,26.295132 16.705119,30.142167 13.943359,33.908203 C13.743445,34.180814 13.612715,34.322738 13.5,34.441406 C13.387285,34.322738 13.256555,34.180814 13.056641,33.908203 C10.284481,30.127985 7.4148684,26.314159 5.015625,22.773438 C2.6163816,19.232715 1,15.953538 1,13.5 C1,6.584596 6.584596,1 13.5,1 Z"></path></g><g transform="translate(6.0, 7.0)" fill="#FFFFFF"></g><g transform="translate(8.0, 8.0)"><circle fill="#000000" opacity="0.25" cx="5.5" cy="5.5" r="5.4999962"></circle><circle fill="#FFFFFF" cx="5.5" cy="5.5" r="5.4999962"></circle></g></g></svg>`
    const marker = new mapbox.Marker(customMarker)
    marker.setLngLat(coordinates)
    marker.addTo(map)
  }

  const handleSearchQuery = () => {
    const coordinates = inputFieldRef.current.dataset.coordinates.split(',')
    if (map) {
      addSearchLocationMarkerToMap(coordinates)
      fetchCinemaLocations(coordinates)

      return
      setTimeout(() => {
        cinemas.forEach(cinema => {
        addCinemaMarkersToMap(cinema)
      })
      }, 0);
    }
  }

  const fetchCinemaLocations = async (coordinates = []) => {
    const queryObj = {
      dataset: 'liste-des-etablissements-cinematographiques-en-france',
      q: '',
      'geofilter.distance': coordinates.join(', ') + ', 5000',
    }
    const query = convertToQuery(queryObj)
    const url = `https://cinema-public.opendatasoft.com/api/records/1.0/search/?${query}`
    const res = await fetch(url)
    const data = await res.json()
    const cinemasArray = data.records.map(theater => {
      return {
        info: theater.fields,
      }
    })
    setCinemas(cinemasArray)
  }

  const addCinemaMarkersToMap = async (cinema) => {
    const cinemaAddress = cinema.info.cinema + ' ' + cinema.info.adresse + ' ' + cinema.info.ville
    const cinemaCoordinates = await fetchAddress()
    if (cinemaCoordinates !== ''){
      const marker = new mapbox.Marker()
      marker.setLngLat(cinemaCoordinates)
      marker.setPopup(new mapbox.Popup().setHTML(`<h6>${cinema.info.nom}</h6s>`))
      marker.addTo(map)
    }

  }

  const convertToQuery = (queryObj) => {
    const queryArray = []
    for (const key in queryObj) {
      if (Object.prototype.hasOwnProperty.call(queryObj, key)) {
        const value = queryObj[key];
        queryArray.push(`${key}=${encodeURIComponent(value)}`)
      }
    }
    return queryArray.join('&')
  }

  const fetchAddress = async () => {
    const query = inputFieldRef.current.value
    if (query.length < 2) return

    addressDropdownRef.current.innerHTML = ''
    const content = {
      query: query,
      hitsPerPage: 5
    }
    const url = `https://places-dsn.algolia.net/1/places/query`
    const res = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(content)
    })
    const data = await res.json()
    data.hits.forEach(location => {
      const listElement = document.createElement('li')
      const locationName = `${location.locale_names.default[0]}, ${location.postcode ? location.postcode[0] : ''} ${location.country.default}` || ''
      const locationCoords = `${location._geoloc.lng}, ${location._geoloc.lat}`
      listElement.appendChild(document.createTextNode(locationName))
      listElement.classList.add('list-group-item', 'list-group-item-action')
      listElement.dataset.coordinates = locationCoords
      listElement.addEventListener('click', () => {
        inputFieldRef.current.value = locationName
        inputFieldRef.current.dataset.coordinates = locationCoords
        addressDropdownRef.current.innerHTML = ''
        console.log(locationCoords)
        handleSearchQuery()
      })
      addressDropdownRef.current.appendChild(listElement)
    })
    const locationCoordinates = data.hits.length === 0 ? '' : data.hits[0]._geoloc
    return locationCoordinates
  }


  return (
    <>
      <div className="container">
        <h1 className='text-center fw-bolder mt-3'>Find a cinema near me</h1>
        <div className="input-group mb-3 position-relative">
          <input
            type="text"
            className="form-control"
            placeholder="Search near address"
            aria-label="Search cinemas"
            aria-describedby="btn-display-cinemas"
            ref={inputFieldRef}
            onChange={fetchAddress}
            data-coordinates = '2.3353,48.8751'
          />
          <button
            className="btn btn-outline-primary"
            type="button"
            id="btn-display-cinemas"
            onClick={handleSearchQuery}
          >Display Cinemas</button>
          <ul className='list-group'  id='addressDropdownRef' ref={addressDropdownRef}></ul>
        </div>
        <div id="map-container" ref={mapContainerRef}></div>
      </div>
    </>
  )
}

export default Page
