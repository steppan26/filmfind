import { useEffect, useState } from "react"
import MovieCard from "../../../components/movieCard"

interface TmdbResponseObject {
  results: Array<Object>
}

const Page = () => {
  const [genres, setGenres] = useState([])
  const [movies, setMovies] = useState([])

  useEffect(() => {
    fetchGenres()
  }, [])

  // fetch list of genres available
  const fetchGenres = async() => {
    const url = 'https://api.themoviedb.org/3/genre/movie/list?api_key=f48b34c17de48f2ea5cc1dcc45f4b07a&language=en-US'
    const res = await fetch(url)
    const data = await res.json()
    setGenres(data.genres)
  }

  // fetch movies based on genres selected
  const fetchMovies = async() => {
    const selectedGenreValue: string = (document.getElementById('genres-list') as HTMLInputElement).value // grab the value for selected option from drop-down
    const selectedGenreId: string | number = genres.filter(genre => genre.name === selectedGenreValue)[0].id // grab the ID for corresponding genre by filtering list of genres
    const url = `https://api.themoviedb.org/3/discover/movie?api_key=f48b34c17de48f2ea5cc1dcc45f4b07a&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${selectedGenreId}`
    const res: Response = await fetch(url)
    const data: TmdbResponseObject = await res.json()
    setMovies(data.results) // update movies state with results received
  }

  return (
    <>
      <div className="container">
        <div className="my-5">
          <h1 className="text-center">Movies by genre</h1>
          <select name="Genres" id="genres-list" className="mt-5 dropdown" onChange={() => fetchMovies()}>
            { genres.map(genre => {
              return (
                <option value={genre.name} key={genre.id}>{genre.name}</option>
              )
            })}
          </select>
        </div>
        <div>
          <ul className="d-flex flex-wrap movies-list">
            {movies.map(movie => {
              return(
                <>
                  <MovieCard movie={movie}/>
                </>
              )
            })}
          </ul>
        </div>
      </div>
    </>
  )
}

export default Page
