import { AppProps } from 'next/app'
import Layout from '../components/layout'
import '../styles/globals.scss'
import Head from "next/head"
import 'bootstrap/dist/css/bootstrap.css'

function MyApp({ Component, pageProps }: AppProps) {

  return (
  <>
    <Head>
      <title>Filmfind - find your joy</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        {/* google fonts */}
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@200;400;700&display=swap" rel="stylesheet" />

        {/* fontaweomse */}
        <script src="https://kit.fontawesome.com/81957c30be.js" crossorigin="anonymous"></script>

        {/* algolia */}
        <script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.5.1/dist/algoliasearch.umd.js"></script>

        {/* mapbox */}
        <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
    </Head>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </>
  )
}

export default MyApp
